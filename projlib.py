import os
import random
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import string

def baralb(size,lnum):
	A = np.zeros((size,size))
	for i in range(3):
		for j in range(i):
			if not i == j :
				A[i,j] = 1
				A[j,i] = 1
	for tail in range(3,size):
		count = 0
		while count < (tail-1) and count < lnum:
			rand = random.random()
			for head in range(tail-1):
				if rand < cumdist(A,head) and A[head,tail] == 0 :
					A[head,tail] = 1
					A[tail,head] = 1
					count += 1
					break
	return A

def cumdist(A,i):
	CDF = 0
	for j in range(i+1):
		CDF += np.sum(A,axis=1)[j]/np.sum(A)
	return CDF

def randwalk(pop,A):
	newpop = pop
	for i in range(len(pop)):
		for n in range(1,int(pop[i])):
			rand=random.random()
			place = 0
			for j in range(len(pop)):
				place += A[i][j]
				if rand<= place/np.sum(A,axis=0)[i]:
					dest = j
					break
			newpop[dest] += 1
			newpop[i] -= 1
	return newpop

def draw_map(adjacency_matrix, mylabels, population):
	gr = nx.Graph()

	# Split links into 4 categories by strength
	links=np.linspace(0,1,4,endpoint=False)
	l=len(links)-1
	rows, cols = np.where(adjacency_matrix !=0)
	edges = zip(rows.tolist(), cols.tolist(),adjacency_matrix[rows,cols])
	gr.add_weighted_edges_from(edges)
	link1 = [(u,v) for (u,v,d) in gr.edges(data=True) if (d['weight']>=links[l])]
	link2 = [(u,v) for (u,v,d) in gr.edges(data=True) if (d['weight']>=links[l-1]) & (d['weight']<links[l])]
	link3 = [(u,v) for (u,v,d) in gr.edges(data=True) if (d['weight']>=links[l-2]) & (d['weight']<links[l-1])]
	link4 = [(u,v) for (u,v,d) in gr.edges(data=True) if (d['weight']>=links[l-3]) & (d['weight']<links[l-2])]

	# Split nodes into 4 categories by population 
	node1 = [i for i in range(len(population)) if (population[i]>=1e4)]
	node2 = [i for i in range(len(population)) if (population[i]>=5e3) & (population[i]<1e4)]
	node3 = [i for i in range(len(population)) if (population[i]>=1e3) & (population[i]<5e3)]
	node4 = [i for i in range(len(population)) if (population[i]>=1e1) & (population[i]<1e3)]

	# Draw Figure
	fig = plt.figure(0)
	pos = nx.spring_layout(gr)
	nx.draw_networkx_nodes(gr,pos,nodelist=node1,node_shape="*", node_color="black", node_size=100)
	nx.draw_networkx_nodes(gr,pos,nodelist=node2,node_shape="D", node_color="black", node_size=75)
	nx.draw_networkx_nodes(gr,pos,nodelist=node3,node_shape="^", node_color="black", node_size=60)
	nx.draw_networkx_nodes(gr,pos,nodelist=node4,node_shape="o", node_color="black", node_size=45)
	nx.draw_networkx_edges(gr,pos,edgelist=link1,width=3,edge_color="blue", alpha=0.75)
	nx.draw_networkx_edges(gr,pos,edgelist=link2,width=2,edge_color="green", alpha=0.9)
	nx.draw_networkx_edges(gr,pos,edgelist=link3,width=2,edge_color="brown",style="dashed",alpha=0.8)
	nx.draw_networkx_edges(gr,pos,edgelist=link4,width=1,edge_color="black",style="dotted")
	
	# For labeling below the node
	offset = -0.10
	pos_labels = {}
	keys = pos.keys()
	for key in keys:
		x, y = pos[key]
		pos_labels[key] = (x, y+offset)
	nx.draw_networkx_labels(gr,pos_labels,labels=mylabels,font_color="black")

	# Draw Graph
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1-0.1,x2+0.1,y1-0.1,y2))
	plt.axis("off")
	plt.tight_layout()
	plt.show(block=False)

	return fig

def make_label_dict(labels):
    l = {}
    for i, label in enumerate(labels):
        l[i] = label
    return l

def randlink(A):
	size = len(A)
	L = np.zeros((size,size))
	for i in range(size):
		for j in range(i,size):
			if i==j :
				L[i,j] = 1
			elif A[i,j] != 0 :
				r = 0
				while r==0 :
					r = random.random()
				L[i,j] = r
				L[j,i] = r
	return L

def namer(size):
	# Open the file in the "read" mode
	THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
	my_file = os.path.join(THIS_FOLDER, 'townnames.csv')
	f = open(my_file, "r")

	# Read in header line
	h = f.readline().split(",")

	# Initialize 2 empty lists
	s0=[]
	s1=[]
	#for i in range(2):
	#	s.append([])

	# Loop over the remaining lines
	for l in f:
		# Create a list by separating the line at commas
		d = l.replace("\n","").split(",")

		# Store the 2 entries in the line to each relevant list
		#for i in range(2):
		#	s[i].append(d[i])
		if len(d[0])>1:
			s0.append(d[0])
		if len(d[1])>1:
			s1.append(d[1])
	# Close the file
	f.close()

	namelist=[]
	for i in range(size):
		namelist.append(s0[random.randint(0,len(s0)-1)]+s1[random.randint(0,len(s1)-1)])
		#print(i)

	return namelist


def atlas_page(namelist,population,adjacency_matrix,i):
	fig = plt.figure()
	l=np.linspace(0,1,4,endpoint=False)
	t=[]
	for j in range(len(namelist)):
		if adjacency_matrix[i,j] > l[len(l)-1] : t.append(str(namelist[j]+" by water"))
		elif adjacency_matrix[i,j] > l[len(l)-2] : t.append(str(namelist[j]+" by highway"))
		elif adjacency_matrix[i,j] > l[len(l)-3] : t.append(str(namelist[j]+" by road"))
		elif adjacency_matrix[i,j] > l[len(l)-4] : t.append(str(namelist[j]+" overland"))

	plt.text(0.5, 0.9, namelist[i], fontsize=18, style='oblique', ha='center', va='top')
	plt.text(0.5, 0.8, "Population: "+str(int(population[i])), ha='center', va='bottom')
	plt.text(0.0, 0.5, "Trade Routes", fontsize=16, ha='left',va='bottom')
	plt.text(0.0, 0.0, "\n".join(t), ha='left',va='bottom',wrap=True)
	plt.text(1.0, 0.5, "Points of Interest", fontsize=16, ha='right',va='bottom')
	plt.text(1.0, 0.0, "\n".join(points_of_interest(population[i])), ha='right',va='bottom',wrap=True)

	plt.axis("off")
	plt.tight_layout()

	return fig

def points_of_interest(pop):
	# Open the file in the "read" mode
	THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
	my_file = os.path.join(THIS_FOLDER, 'poi.csv')
	f = open(my_file, "r")

	# Read in header line
	h = f.readline().split(",")

	# Initialize 2 empty lists
	bus=[]
	SV=[]

	# Loop over the remaining lines
	for l in f:
		# Create a list by separating the line at commas
		d = l.replace("\n","").split(",")

		# Store the 2 entries in the line to each relevant list
		bus.append(d[0])
		SV.append(int(d[1]))
	
	# Close the file
	f.close()

	loc=[]
	for i in range(len(SV)):
		if pop >= 2*SV[i] :
			loc.append(str(int(pop/SV[i]))+" "+bus[i]+"s")
		elif pop >= SV[i] :
			loc.append(str(1)+" "+bus[i])
		elif random.random()<pop/SV[i] :
			loc.append(str(1)+" "+bus[i])
		#print(i)
	return loc
