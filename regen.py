import numpy as np
import matplotlib.pyplot as plt
import string
from matplotlib.backends.backend_pdf import PdfPages
from projlib import *

# Generate network
size = int(input('How many towns should there be? '))
lnum = int(input('How many routes should be added with each new town? '))
A = baralb(size, lnum)
A = np.array(randlink(A))
A += np.diag(np.ones(size))

# Initial Conditions
pop = np.zeros(size)
pop[0] += size*10

# Time Evolution
tmax = 2*size
dt = 1
time = np.linspace(dt,tmax,tmax/dt)
for t in range(1,tmax) :
	pop = list(randwalk(pop,A))

# Name each town and name region for largest town
namelist=namer(size)
reg_name = str(namelist[pop.index(max(pop))])

# Convert Percent of Total Population to Actual Population
#totpop = int(input('What should the total population be? '))
pop=np.array(pop)*100#totpop/(10*size)

# Draw map and atlas pages
fig = draw_map(A-np.diag(np.ones(size)), make_label_dict(namelist),pop)
pp = PdfPages(reg_name + '.pdf')
pp.savefig(fig)
for i in range(size):
	pp.savefig(atlas_page(namelist,pop,A-np.diag(np.ones(size)),i))
pp.close()
print('Saved Region of ',reg_name)

# Print Final Population
for i in range(len(pop)):
	print('The population of ',namelist[i],' is: ', int(pop[i]))

plt.show()