# regen

Fantasy Region Generator Written in Python

Description:   
Program accepts a size from user and generates a scale free network with that number of nodes.  Link strength is randomized, with the strongest link always being the current position.  A population is then dropped and allowed to randomly walk for several cycles.   
The program then names each node, plots the graph, and prints a quasi-random description.  Results are displayed and saved to a PDF.   
Best Results are obtained for regions of less than 20 towns with only one or two connections added per town.  

Requirements:  
The code explicitly depends on the networkx, matplotlib, and numpy python packages and all their dependencies, which may not be included in all distributions.  
Using "pip install -r requirements.txt" should install all the required packages.

Reading Results:  
The graph represents four different types of connection:  
Solid Blue lines are the strongest links, representing river or coastal routes  
Solid Green lines are the next strongest, representing highways or paved roads  
Dashed Brown lines are weaker connections, indicating side roads  
Dotted Black lines are the weakest connection, and represent overland routes  
  
The graph shows four different node symbols:  
Stars indicated a metropolis with population greater than 10,000  
Diamonds represent a city with population greater than 5,000  
Triangles represent towns with population greater than 1,000  
Circles represent villages with population less than 1,000  

If a node's population is zero then no symbol appears, though it will still be labelled as a possible point of interest.

Files:  
regen.py is the main file, run in terminal
projlib.py is a library of all the functions called by regen.py  
requirements.txt should include all Python packages needed to run the code
Treewill.pdf is a sample of the program's output
townnames.csv and poi.csv contain information used by the program, see below


Changing the Names:  
Two *.csv files are attached: townnames.csv and poi.csv  
These files must be present but they can be easily changed.  Adding or removing lines alters changes the output of names or local points of interest.  
townnames.csv includes two columns, the first part of a name and the second part.  Town Names are generated by randomly taking two and connecting them.  
poi.csv adds local points of interest based on the node's population, with the point of interest in the left column and the necessary population in the right.  If a node doesn't have the necessary population then it still has a ranodm chance of the item appearing.  

townnames.csv based on this: https://www.reddit.com/r/DnD/comments/8ifhqk/oc_town_name_creation_helper/   
poi.csv and the population numbers based on: http://www222.pair.com/sjohn/blueroom/demog.htm   